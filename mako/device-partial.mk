# Copyright 2013 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Qualcomm blob(s) necessary for Mako hardware
PRODUCT_COPY_FILES := \
    vendor/qcom/mako/proprietary/bin/ATFWD-daemon:system/vendor/bin/ATFWD-daemon:qcom \
    vendor/qcom/mako/proprietary/bin/bridgemgrd:system/vendor/bin/bridgemgrd:qcom \
    vendor/qcom/mako/proprietary/bin/btnvtool:system/vendor/bin/btnvtool:qcom \
    vendor/qcom/mako/proprietary/bin/diag_klog:system/vendor/bin/diag_klog:qcom \
    vendor/qcom/mako/proprietary/bin/diag_mdlog:system/vendor/bin/diag_mdlog:qcom \
    vendor/qcom/mako/proprietary/bin/ds_fmc_appd:system/vendor/bin/ds_fmc_appd:qcom \
    vendor/qcom/mako/proprietary/bin/efsks:system/vendor/bin/efsks:qcom \
    vendor/qcom/mako/proprietary/bin/hci_qcomm_init:system/vendor/bin/hci_qcomm_init:qcom \
    vendor/qcom/mako/proprietary/bin/ks:system/vendor/bin/ks:qcom \
    vendor/qcom/mako/proprietary/bin/mm-qcamera-daemon:system/vendor/bin/mm-qcamera-daemon:qcom \
    vendor/qcom/mako/proprietary/bin/mpdecision:system/vendor/bin/mpdecision:qcom \
    vendor/qcom/mako/proprietary/bin/netmgrd:system/vendor/bin/netmgrd:qcom \
    vendor/qcom/mako/proprietary/bin/nl_listener:system/vendor/bin/nl_listener:qcom \
    vendor/qcom/mako/proprietary/bin/port-bridge:system/vendor/bin/port-bridge:qcom \
    vendor/qcom/mako/proprietary/bin/qcks:system/vendor/bin/qcks:qcom \
    vendor/qcom/mako/proprietary/bin/qmuxd:system/vendor/bin/qmuxd:qcom \
    vendor/qcom/mako/proprietary/bin/qseecomd:system/vendor/bin/qseecomd:qcom \
    vendor/qcom/mako/proprietary/bin/radish:system/vendor/bin/radish:qcom \
    vendor/qcom/mako/proprietary/bin/sensors.qcom:system/vendor/bin/sensors.qcom:qcom \
    vendor/qcom/mako/proprietary/bin/thermald:system/vendor/bin/thermald:qcom \
    vendor/qcom/mako/proprietary/bin/usbhub:system/vendor/bin/usbhub:qcom \
    vendor/qcom/mako/proprietary/bin/usbhub_init:system/vendor/bin/usbhub_init:qcom \
    vendor/qcom/mako/proprietary/bin/v4l2-qcamera-app:system/vendor/bin/v4l2-qcamera-app:qcom \
    vendor/qcom/mako/proprietary/firmware/tzapps.b00:system/vendor/firmware/tzapps.b00:qcom \
    vendor/qcom/mako/proprietary/firmware/tzapps.b01:system/vendor/firmware/tzapps.b01:qcom \
    vendor/qcom/mako/proprietary/firmware/tzapps.b02:system/vendor/firmware/tzapps.b02:qcom \
    vendor/qcom/mako/proprietary/firmware/tzapps.b03:system/vendor/firmware/tzapps.b03:qcom \
    vendor/qcom/mako/proprietary/firmware/tzapps.mdt:system/vendor/firmware/tzapps.mdt:qcom \
    vendor/qcom/mako/proprietary/firmware/vidc_1080p.fw:system/vendor/firmware/vidc_1080p.fw:qcom \
    vendor/qcom/mako/proprietary/firmware/vidc.b00:system/vendor/firmware/vidc.b00:qcom \
    vendor/qcom/mako/proprietary/firmware/vidc.b01:system/vendor/firmware/vidc.b01:qcom \
    vendor/qcom/mako/proprietary/firmware/vidc.b02:system/vendor/firmware/vidc.b02:qcom \
    vendor/qcom/mako/proprietary/firmware/vidc.b03:system/vendor/firmware/vidc.b03:qcom \
    vendor/qcom/mako/proprietary/firmware/vidc.mdt:system/vendor/firmware/vidc.mdt:qcom \
    vendor/qcom/mako/proprietary/lib/libplayback_adreno.so:system/vendor/lib/egl/libplayback_adreno.so:qcom \
    vendor/qcom/mako/proprietary/lib/flp.msm8960.so:system/vendor/lib/hw/flp.msm8960.so:qcom \
    vendor/qcom/mako/proprietary/lib/sensors.mako.so:system/vendor/lib/sensors.mako.so:qcom \
    vendor/qcom/mako/proprietary/lib/libaudcal.so:system/vendor/lib/libaudcal.so:qcom \
    vendor/qcom/mako/proprietary/lib/libaudioalsa.so:system/vendor/lib/libaudioalsa.so:qcom \
    vendor/qcom/mako/proprietary/lib/libc2d30.so:system/vendor/lib/libc2d30.so:qcom \
    vendor/qcom/mako/proprietary/lib/libchromatix_imx111_default_video.so:system/vendor/lib/libchromatix_imx111_default_video.so:qcom \
    vendor/qcom/mako/proprietary/lib/libchromatix_imx111_preview.so:system/vendor/lib/libchromatix_imx111_preview.so:qcom \
    vendor/qcom/mako/proprietary/lib/libchromatix_imx111_zsl.so:system/vendor/lib/libchromatix_imx111_zsl.so:qcom \
    vendor/qcom/mako/proprietary/lib/libchromatix_imx119_default_video.so:system/vendor/lib/libchromatix_imx119_default_video.so:qcom \
    vendor/qcom/mako/proprietary/lib/libchromatix_imx119_preview.so:system/vendor/lib/libchromatix_imx119_preview.so:qcom \
    vendor/qcom/mako/proprietary/lib/libchromatix_mt9e013_default_video.so:system/vendor/lib/libchromatix_mt9e013_default_video.so:qcom \
    vendor/qcom/mako/proprietary/lib/libchromatix_mt9e013_preview.so:system/vendor/lib/libchromatix_mt9e013_preview.so:qcom \
    vendor/qcom/mako/proprietary/lib/libchromatix_mt9e013_video_hfr.so:system/vendor/lib/libchromatix_mt9e013_video_hfr.so:qcom \
    vendor/qcom/mako/proprietary/lib/libchromatix_ov5647_default_video.so:system/vendor/lib/libchromatix_ov5647_default_video.so:qcom \
    vendor/qcom/mako/proprietary/lib/libchromatix_ov5647_preview.so:system/vendor/lib/libchromatix_ov5647_preview.so:qcom \
    vendor/qcom/mako/proprietary/lib/libchromatix_ov5647_video_hfr.so:system/vendor/lib/libchromatix_ov5647_video_hfr.so:qcom \
    vendor/qcom/mako/proprietary/lib/libchromatix_ov8825_default_video.so:system/vendor/lib/libchromatix_ov8825_default_video.so:qcom \
    vendor/qcom/mako/proprietary/lib/libchromatix_ov8825_preview.so:system/vendor/lib/libchromatix_ov8825_preview.so:qcom \
    vendor/qcom/mako/proprietary/lib/libchromatix_ov9726_default_video.so:system/vendor/lib/libchromatix_ov9726_default_video.so:qcom \
    vendor/qcom/mako/proprietary/lib/libchromatix_ov9726_preview.so:system/vendor/lib/libchromatix_ov9726_preview.so:qcom \
    vendor/qcom/mako/proprietary/lib/libchromatix_s5k4e1_default_video.so:system/vendor/lib/libchromatix_s5k4e1_default_video.so:qcom \
    vendor/qcom/mako/proprietary/lib/libchromatix_s5k4e1_preview.so:system/vendor/lib/libchromatix_s5k4e1_preview.so:qcom \
    vendor/qcom/mako/proprietary/lib/libchromatix_vx6953_default_video.so:system/vendor/lib/libchromatix_vx6953_default_video.so:qcom \
    vendor/qcom/mako/proprietary/lib/libchromatix_vx6953_preview.so:system/vendor/lib/libchromatix_vx6953_preview.so:qcom \
    vendor/qcom/mako/proprietary/lib/libCommandSvc.so:system/vendor/lib/libCommandSvc.so:qcom \
    vendor/qcom/mako/proprietary/lib/libconfigdb.so:system/vendor/lib/libconfigdb.so:qcom \
    vendor/qcom/mako/proprietary/lib/libcsd-client.so:system/vendor/lib/libcsd-client.so:qcom \
    vendor/qcom/mako/proprietary/lib/libdiag.so:system/vendor/lib/libdiag.so:qcom \
    vendor/qcom/mako/proprietary/lib/libdrmdiag.so:system/vendor/lib/libdrmdiag.so:qcom \
    vendor/qcom/mako/proprietary/lib/libdrmfs.so:system/vendor/lib/libdrmfs.so:qcom \
    vendor/qcom/mako/proprietary/lib/libdrmtime.so:system/vendor/lib/libdrmtime.so:qcom \
    vendor/qcom/mako/proprietary/lib/libdsi_netctrl.so:system/vendor/lib/libdsi_netctrl.so:qcom \
    vendor/qcom/mako/proprietary/lib/libdsprofile.so:system/vendor/lib/libdsprofile.so:qcom \
    vendor/qcom/mako/proprietary/lib/libdss.so:system/vendor/lib/libdss.so:qcom \
    vendor/qcom/mako/proprietary/lib/libdsucsd.so:system/vendor/lib/libdsucsd.so:qcom \
    vendor/qcom/mako/proprietary/lib/libdsutils.so:system/vendor/lib/libdsutils.so:qcom \
    vendor/qcom/mako/proprietary/lib/libgemini.so:system/vendor/lib/libgemini.so:qcom \
    vendor/qcom/mako/proprietary/lib/libI420colorconvert.so:system/vendor/lib/libI420colorconvert.so:qcom \
    vendor/qcom/mako/proprietary/lib/libidl.so:system/vendor/lib/libidl.so:qcom \
    vendor/qcom/mako/proprietary/lib/libimage-jpeg-dec-omx-comp.so:system/vendor/lib/libimage-jpeg-dec-omx-comp.so:qcom \
    vendor/qcom/mako/proprietary/lib/libimage-jpeg-enc-omx-comp.so:system/vendor/lib/libimage-jpeg-enc-omx-comp.so:qcom \
    vendor/qcom/mako/proprietary/lib/libimage-omx-common.so:system/vendor/lib/libimage-omx-common.so:qcom \
    vendor/qcom/mako/proprietary/lib/libmercury.so:system/vendor/lib/libmercury.so:qcom \
    vendor/qcom/mako/proprietary/lib/libmmcamera_faceproc.so:system/vendor/lib/libmmcamera_faceproc.so:qcom \
    vendor/qcom/mako/proprietary/lib/libmmcamera_frameproc.so:system/vendor/lib/libmmcamera_frameproc.so:qcom \
    vendor/qcom/mako/proprietary/lib/libmmcamera_hdr_lib.so:system/vendor/lib/libmmcamera_hdr_lib.so:qcom \
    vendor/qcom/mako/proprietary/lib/libmmcamera_image_stab.so:system/vendor/lib/libmmcamera_image_stab.so:qcom \
    vendor/qcom/mako/proprietary/lib/libmmcamera_statsproc31.so:system/vendor/lib/libmmcamera_statsproc31.so:qcom \
    vendor/qcom/mako/proprietary/lib/libmmcamera_wavelet_lib.so:system/vendor/lib/libmmcamera_wavelet_lib.so:qcom \
    vendor/qcom/mako/proprietary/lib/libmm-color-convertor.so:system/vendor/lib/libmm-color-convertor.so:qcom \
    vendor/qcom/mako/proprietary/lib/libmmipl.so:system/vendor/lib/libmmipl.so:qcom \
    vendor/qcom/mako/proprietary/lib/libmmjpeg.so:system/vendor/lib/libmmjpeg.so:qcom \
    vendor/qcom/mako/proprietary/lib/libmmjps.so:system/vendor/lib/libmmjps.so:qcom \
    vendor/qcom/mako/proprietary/lib/libmmmpod.so:system/vendor/lib/libmmmpod.so:qcom \
    vendor/qcom/mako/proprietary/lib/libmmmpo.so:system/vendor/lib/libmmmpo.so:qcom \
    vendor/qcom/mako/proprietary/lib/libmmstillomx.so:system/vendor/lib/libmmstillomx.so:qcom \
    vendor/qcom/mako/proprietary/lib/libnetmgr.so:system/vendor/lib/libnetmgr.so:qcom \
    vendor/qcom/mako/proprietary/lib/liboemcamera.so:system/vendor/lib/liboemcamera.so:qcom \
    vendor/qcom/mako/proprietary/lib/liboemcrypto.so:system/vendor/lib/liboemcrypto.so:qcom \
    vendor/qcom/mako/proprietary/lib/libqcci_legacy.so:system/vendor/lib/libqcci_legacy.so:qcom \
    vendor/qcom/mako/proprietary/lib/libqdi.so:system/vendor/lib/libqdi.so:qcom \
    vendor/qcom/mako/proprietary/lib/libqdp.so:system/vendor/lib/libqdp.so:qcom \
    vendor/qcom/mako/proprietary/lib/libqmi_cci.so:system/vendor/lib/libqmi_cci.so:qcom \
    vendor/qcom/mako/proprietary/lib/libqmi_client_qmux.so:system/vendor/lib/libqmi_client_qmux.so:qcom \
    vendor/qcom/mako/proprietary/lib/libqmi_common_so.so:system/vendor/lib/libqmi_common_so.so:qcom \
    vendor/qcom/mako/proprietary/lib/libqmi_csi.so:system/vendor/lib/libqmi_csi.so:qcom \
    vendor/qcom/mako/proprietary/lib/libqmi_csvt_srvc.so:system/vendor/lib/libqmi_csvt_srvc.so:qcom \
    vendor/qcom/mako/proprietary/lib/libqmi_encdec.so:system/vendor/lib/libqmi_encdec.so:qcom \
    vendor/qcom/mako/proprietary/lib/libqmiservices.so:system/vendor/lib/libqmiservices.so:qcom \
    vendor/qcom/mako/proprietary/lib/libqmi.so:system/vendor/lib/libqmi.so:qcom \
    vendor/qcom/mako/proprietary/lib/libQSEEComAPI.so:system/vendor/lib/libQSEEComAPI.so:qcom \
    vendor/qcom/mako/proprietary/lib/libril-qc-qmi-1.so:system/vendor/lib/libril-qc-qmi-1.so:qcom \
    vendor/qcom/mako/proprietary/lib/libril-qcril-hook-oem.so:system/vendor/lib/libril-qcril-hook-oem.so:qcom \
    vendor/qcom/mako/proprietary/lib/libsensor1.so:system/vendor/lib/libsensor1.so:qcom \
    vendor/qcom/mako/proprietary/lib/libsensor_reg.so:system/vendor/lib/libsensor_reg.so:qcom \
    vendor/qcom/mako/proprietary/lib/libsensor_user_cal.so:system/vendor/lib/libsensor_user_cal.so:qcom \
    vendor/qcom/mako/proprietary/lib/libstagefright_hdcp.so:system/vendor/lib/libstagefright_hdcp.so:qcom \
    vendor/qcom/mako/proprietary/lib/libxml.so:system/vendor/lib/libxml.so:qcom \
    vendor/qcom/mako/proprietary/firmware/a300_pfp.fw:system/vendor/firmware/a300_pfp.fw:qcom \
    vendor/qcom/mako/proprietary/firmware/a300_pm4.fw:system/vendor/firmware/a300_pm4.fw:qcom \
    vendor/qcom/mako/proprietary/firmware/dsps.b00:system/vendor/firmware/dsps.b00:qcom \
    vendor/qcom/mako/proprietary/firmware/dsps.b01:system/vendor/firmware/dsps.b01:qcom \
    vendor/qcom/mako/proprietary/firmware/dsps.b02:system/vendor/firmware/dsps.b02:qcom \
    vendor/qcom/mako/proprietary/firmware/dsps.b03:system/vendor/firmware/dsps.b03:qcom \
    vendor/qcom/mako/proprietary/firmware/dsps.b04:system/vendor/firmware/dsps.b04:qcom \
    vendor/qcom/mako/proprietary/firmware/dsps.b05:system/vendor/firmware/dsps.b05:qcom \
    vendor/qcom/mako/proprietary/firmware/dsps.mdt:system/vendor/firmware/dsps.mdt:qcom \
    vendor/qcom/mako/proprietary/firmware/q6.b00:system/vendor/firmware/q6.b00:qcom \
    vendor/qcom/mako/proprietary/firmware/q6.b01:system/vendor/firmware/q6.b01:qcom \
    vendor/qcom/mako/proprietary/firmware/q6.b03:system/vendor/firmware/q6.b03:qcom \
    vendor/qcom/mako/proprietary/firmware/q6.b04:system/vendor/firmware/q6.b04:qcom \
    vendor/qcom/mako/proprietary/firmware/q6.b05:system/vendor/firmware/q6.b05:qcom \
    vendor/qcom/mako/proprietary/firmware/q6.b06:system/vendor/firmware/q6.b06:qcom \
    vendor/qcom/mako/proprietary/firmware/q6.mdt:system/vendor/firmware/q6.mdt:qcom \
    vendor/qcom/mako/proprietary/firmware/wcnss.b00:system/vendor/firmware/wcnss.b00:qcom \
    vendor/qcom/mako/proprietary/firmware/wcnss.b01:system/vendor/firmware/wcnss.b01:qcom \
    vendor/qcom/mako/proprietary/firmware/wcnss.b02:system/vendor/firmware/wcnss.b02:qcom \
    vendor/qcom/mako/proprietary/firmware/wcnss.b04:system/vendor/firmware/wcnss.b04:qcom \
    vendor/qcom/mako/proprietary/firmware/wcnss.b05:system/vendor/firmware/wcnss.b05:qcom \
    vendor/qcom/mako/proprietary/firmware/wcnss.mdt:system/vendor/firmware/wcnss.mdt:qcom \
    vendor/qcom/mako/proprietary/lib/libdrmdecrypt.so:system/vendor/lib/libdrmdecrypt.so:qcom \
    vendor/qcom/mako/proprietary/lib/libgeofence.so:system/vendor/lib/libgeofence.so:qcom \
    vendor/qcom/mako/proprietary/lib/libizat_core.so:system/vendor/lib/libizat_core.so:qcom \
    vendor/qcom/mako/proprietary/lib/libloc_api_v02.so:system/vendor/lib/libloc_api_v02.so:qcom \
    vendor/qcom/mako/proprietary/lib/libloc_ds_api.so:system/vendor/lib/libloc_ds_api.so:qcom \

PRODUCT_PACKAGES := libacdbloader

$(call inherit-product, vendor/qcom/msm8960/graphics/graphics-vendor.mk)
